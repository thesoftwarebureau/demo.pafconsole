﻿using System;
using SoftwareBureau.PAF;

namespace PAFConsoleDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("PAF Example");

            // Create a settings object
            PAFSettings pafSettings = new PAFSettings()
            {
                AddressEnhancement = true,
                AdjustPostcodePartialMatches = true,
                PostcodeEnhancement = true,
                IncludeCounties = true
            };

            // Setup our PAF Object, note it supports IDisposable interface

            using (MatchPAF matchPAF = new MatchPAF("PAF", "any value you want", @"..\..\db\BurstResources.bin", pafSettings))
            {
                // output some version info
                Console.WriteLine($"PAF Loaded: {matchPAF.APIVersion}, {matchPAF.ServerVersion}, {matchPAF.DataSets}, {matchPAF.DaysLeft}");

                string address = null;
                do
                {
                    Console.Write("Enter a comma separated address (x to leave):");

                    address = Console.ReadLine();

                    if (address == "x") continue;
                    if (string.IsNullOrEmpty(address)) continue;

                    // perform a search
                    PAFResults pafResults = matchPAF.Search(address);

                    // check we have a result
                    if (pafResults.OutputAddress.ResultStatus != ResCode.Success)
                    {
                        Console.WriteLine($"Country {pafResults.InputAddress.Country}");
                        continue;
                    }

                    // display some of the results
                    Console.WriteLine($"Enhanced Core Address: {pafResults.OutputAddress.EnhancedAddress.Core}");
                    Console.WriteLine($"Enhanced Town Address: {pafResults.OutputAddress.EnhancedAddress.Town}");
                    Console.WriteLine($"Enhanced County Address: {pafResults.OutputAddress.EnhancedAddress.County}");
                    Console.WriteLine($"Enhanced Postcode Address: {pafResults.OutputAddress.EnhancedAddress.Zip}");
                    Console.WriteLine($"Enhanced Status: {pafResults.OutputAddress.EnhancedAddress.MatchStatus}");

                    Console.WriteLine();
                    Console.WriteLine("Enhanced Address Label:...");
                    Console.WriteLine($"{pafResults.OutputAddress.EnhancedAddress.Label}");

                    Console.WriteLine($"United Kingdom {pafResults.InputAddress.Country == "UNITED KINGDOM"}");
                    Console.WriteLine($"Country selection {pafResults.InputAddress.CountrySelectionReason}");

                } while (address != "x");

            } // disposes matchPAF
        }
    }
}

