# PAF API Demo 1

## The PAF API Console Application
* This repo holds an application that demonstrates how to use the PAF API
* For brevity all parameters are hard-coded
* This is not intended as production ready code but is intended for demonstration purposes

## Steps Required To Run The Console Application

### Pre-requisite
* You must have git installed and a git client such as Git Bash (install from https://git-scm.com/)
* If you prefer we can email the demonstration application

### Step 1 Credentials & Resources
* Request a capscn02.lic file
* Link to download PAF resources
* A PAF API Product key and
* Credentials for the TSB Nuger server

from The Software Bureau Limited at support@thesoftwarebureau.com

### Step 2 Download PAF Resources following instructions in your Email
* The rest of this guide assumes these resources are copied to D:\PAF\Resources
* and that the capscn02.lic file received at step 1 has been copied to D:\PAF\Licence

You can continue with these instructions whilst waiting for the download to complete

### Step 3 Connect Visual Studio to The Software Bureau Nuget Server
* Download latest nuget.exe : https://dist.nuget.org/win-x86-commandline/latest/nuget.exe
* Add TSB as a package feed source from the command line as follows:
	* nuget sources Add -Name "TSB" -Source "https://nuget.thesoftwarebureau.com/nuget"
	* nuget sources Update -Name "TSB" -UserName "UserName" -Password "YOUR_PASSWORD"

![alternativetext](Images/NuGet-Add.png)

* The changes will be saved to your machines configuration file: %appdata%\nuget\NuGet.Config
* Open Visual studio
	* Tools/Options .. NuGet Package Manager
	* You should see TSB listed as a package source

![alternativetext](Images/TSB-As-Packet-Source.png)

### Step 4 Git Pull
* Open Git Bash in your projects directory (install from https://git-scm.com/)
* git clone https://bitbucket.org/thesoftwarebureau/demo.pafconsole.git
* or use your favourite git client to clone the PAFConsoleDemo solution

![alternativetext](Images/Git-Bash-Clone.png)

### Step 5 Visual Studio
* Open the PAFConsoleDemo solution in visual studio
* Ensure the selected package soruce is TSB and install the following packages
* SoftwareBureau.Core.Burst.Data.Resource
* SoftwareBureau.PAF

![alternativetext](Images/Install-TSB-Nuget-Packages.png)

* You will need to accept the licence agreement to install the API
* Your solution will now look like this

![alternativetext](Images/Final-Project.png)

* The project should will now build but you still need to license the PAF API before you run the project

### Step 6 PAF Resources

Double click spider.ini to edit the contents and replace the following key values with the location of the files copied to your system at step 2

* Location of capscn02.lic LicPath=D:\PAF\Licence
* Location of capscan.paf PAF=D:\PAF\Resources\capscan.paf
* Location of extra files RCDB=D:\PAF\Resources\

### Step 7 License Key
* Launce the application SoftwareBureau.PAF.Activate.exe from the project folder
* Press OK in the next window displayed to accept the SystemSettings file

![alternativetext](Images/Settings-File-Accept.png)

* When requested to select Licence Type select "Activation"

![alternativetext](Images/Licence-Type-Activation.png)

* Enter the API Product Key you recieved at Step 1 in the text box and click the Activate button
* Select the following files in Solution Explorer and set "Copy to Output Directory" to "Copy if newer"
	* spider.ini
	* cpsvrmc5.dll
	* McEng.dll
	* mcclient.dll
    * TurboActivate.dat
    * TurboActivate64.dll

### Step 8 Run the Application
* Run the console application
* The console application will prompt the user to enter an input comma delimited address
* When the user presses the enter key the PAF result is displayed

![alternativetext](Images/Run-Console-Application.png)

## Some Important Notes When Creating an Application from Scratch
* The application must be 64 bit only
*  useLegacyV2RuntimeActivationPolicy="true" must be added to the startUp tag of app.config

## API Documentation

### Search and Print an Address Label

````C#
// perform a search
PAFResults pafResults = matchPAF.Search("The Cottage,Woodbreach Drive,LE16");

// check we have a result and return if we do not have a result
if (pafResults.OutputAddress.ResultStatus != ResCode.Success) return;

// output the official PAF address
Console.WriteLine(pafResults.OutputAddress.Label);

// output the Software Bureau Enhanced address - we put back vanity information
Console.WriteLine(pafResults.OutputAddress.EnhancedAddress.Label)

````
For the example above the Label would look like the following - note the Enhanced PAF Label retains "The Cottage"

Official PAF Label

    Woodbreach Drive
    Market Harborough
    LE16 7XG

Enhanced PAF Label

    The Cottage
    Woodbreach Drive
    Market Harborough
    LE16 7XG

### Print a Delivery Point Suffix & Unique Delivery Point Reference Number

````C#
// perform a search
PAFResults pafResults = matchPAF.Search("The Cottage,10 Woodbreach Drive,LE16");

// check we have a result and return if we do not have a result
if (pafResults.OutputAddress.ResultStatus != ResCode.Success) return;

// output the official PAF address
Console.WriteLine($"DPS: {pafResults.OutputAddress.DPS} UDPRN: {pafResults.OutputAddress.UDPRN}");

````
For the example above the output would be as follows...

    DPS: 2Q UDPRN: 13379614


### Has the Record Been Idenfified as Particular Country

Console.WriteLine($"Country is UK: {pafResults.InputAddress.Country == "UNITED KINGDOM"});
Console.WriteLine($"Country is France: {pafResults.InputAddress.Country == "FRANCE"});


### PAF Status Fields

A number of status fields are output by PAF

````C#
// perform a search
PAFResults pafResults = matchPAF.Search("The Cottage,10 Woodbreach Drive,LE16");

// check we have a result and return if we do not have a result
if (pafResults.OutputAddress.ResultStatus != ResCode.Success) return;

// True - 
Console.WriteLine($"MOI: {pafResults.OutputAddress.MOI}");

// NoMatch - no PAF match
// Town - match to Town but not to Street
// Street - match to Street but not to Premise 
// Premise - match to Premise
// Organisation - match to Orgainisation
Console.WriteLine($"Match Level: {pafResults.OutputAddress.MatchLevel}");

// 0..100 Match Confidence Score - Below 50 indicates need manual confirmation
Console.WriteLine($"Match Score: {pafResults.OutputAddress.MatchScore}");

// True - the building matched to has multiple homes
Console.WriteLine($"Multi residency: {pafResults.OutputAddress.MultiResidency}");

// NotCrossMatch = 32
// Corrected = 67
// NotMatched = 78
// Parsed = 80
// Verified = 86
Console.WriteLine($"Output Status: {pafResults.OutputAddress.OutputStatus}");

// PostcodeStatus
// No Postcode = X
// Fully Postcoded = F
// Outbound Only = P
Console.WriteLine($"PostCode Change Level: {pafResults.OutputAddress.OutputPostCodeLevel}");

// Area
// NoPostcode
// InputPostcodeNoOutputPostcode
// OutputPostcodeNoInputPostcode
// NoChange
// Outcode
// Postcode
// Sector
Console.WriteLine($"PostCode Change Level: {pafResults.OutputAddress.PostCodeChangeLevel}");

// Error
// Success
// NoHits
// Insufficient
// Ambiguous
// Foreign
Console.WriteLine($"Result Status: {pafResults.OutputAddress.ResultStatus}");

// UsedFullPostcode
// UsedOutCodeOnly
Console.WriteLine($"Result Status: {pafResults.OutputAddress.RetryOutcodeOnlyStatus}");

// Undefined - for Country not yet defined
// Level1Credentials - eg for UK BFPO
// CountryInLastLine
// Level2Credentials - eg for UK Postcode found
// CountryInPenultimateLine
// Level3Credentials - eg for UK known province or county
// DependencyLastLine - eg for Australia Tazmania 
// DependencyPenultimateLine - eg for UK ASCENSION ISLAND or TRISTAN DA CUNHA
// TraceElement - eg for ROI Dublin
// EmbeddedCountry - country found on another line
// DefaultCountry - normally United Kingdom
Console.WriteLine($"Source of Country: {pafResults.InputAddress.CountrySelectionTier}");

````